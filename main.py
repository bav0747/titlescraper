#! python
import sys
import os
import re

import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

output_filename = "output.txt"

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print("Bitte Liste der URLs als Datei übergeben.")
        quit(1)

    path = sys.argv[1]

    with open(path, "r") as f:
        urls = f.read().splitlines()

    if len(urls) < 1:
        print("Keine URLs in Datei gefunden oder Datei nicht lesbar.")
        quit(1)
    
    titles = []

    if os.path.exists(output_filename):
        os.remove(output_filename)

    for i in tqdm(range(len(urls))):
        url = urls[i]
        if not url.startswith(("http://", "https://")):
            url = "http://" + url
        try:
            res = requests.get(url, timeout=2)
            res.encoding = res.apparent_encoding
            if 200 <= res.status_code < 300:
                soup = BeautifulSoup(res.text, 'html.parser')
                title = soup.find("title").string.strip()
                title = re.sub("\s", " ", title)
            else:
                raise Exception
        except:
            title = "--- Titel konnte nicht gescraped werden. ---"

        titles.append(title+"\n")
        
        if (i%10==0 and i>0) or i==len(urls)-1:
            with open(output_filename, "a",encoding="utf-8") as f:
                f.writelines(titles)
                titles = []
            
