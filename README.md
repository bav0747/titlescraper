# titleScraper

Collects content of title-tags from given list of urls.

# setup

    python -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

